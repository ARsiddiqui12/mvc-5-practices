﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using booklynk.Models;

namespace booklynk.ViewModels
{
    public class RandomBooksViewModel
    {

        public Books Books { get; set; }

        public List<Customer> Customer { get; set; }

        public List<Author> Auther { get; set; }

    }
}