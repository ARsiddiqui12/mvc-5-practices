﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using booklynk.Models;
using booklynk.ViewModels;

namespace booklynk.Controllers
{
    public class BooksController : Controller
    {
        

        // GET: Books /random books
        public ActionResult Random()
        {
            
            var books = new Books()
            {
                Id =1,
                Name = "bookone"
            };

            var customers = new List<Customer>
            {

                new Customer {Id=1,Name="Customer 1"},
                new Customer {Id=2,Name="Customer 2"},
                new Customer {Id=3,Name="Customer 3"},
                new Customer {Id=4,Name="Customer 4"}


            };

            var author = new List<Author>
            {

                new Author
                {
                    Id = 1,
                    Name = "Author 1",
                    Age =55
                },
                new Author
                {
                    Id = 2,
                    Name = "Author 2",
                    Age =56
                },
                new Author
                {
                    Id = 3,
                    Name = "Author 3",
                    Age =57
                },
                new Author
                {
                    Id = 4,
                    Name = "Author 4",
                    Age =58
                },

            };

            var viewModel = new RandomBooksViewModel
            {

                Books = books,
                Customer = customers,
                Auther = author
            };

            return View(viewModel);




            //return Content("hello world");

            //return HttpNotFound();

            //return new EmptyResult();

            //return RedirectToAction("index","Home", new { page = 1, sortBy ="name" });
        }

        
        public ActionResult Editdata(int id = 0)
        {

            return Content(content: "id :" + id);

        }

        public ActionResult Index(int? pageIndex,string sortBy)
        {

            if (!pageIndex.HasValue)

                pageIndex = 1;

            if (String.IsNullOrWhiteSpace(sortBy))

                sortBy = "name";

            return Content(String.Format("pageIndex={0}&sortBy={1}",pageIndex,sortBy));



        }


        [Route("books/publish/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult BooksbyPublished(int year, int month)
        {

            return Content(year + "/" + month);

        }


    }
}